# Personal Website
This repository contains source code for my personal website. My personal website is availabe on this [Link](https://www.pradiptagitaya.xyz)  
Feel free to visit it

### Pipeline Status
[![pipeline status](https://gitlab.com/rd.pradipta/personal-web/badges/master/pipeline.svg)](https://gitlab.com/rd.pradipta/personal-web/crisis/commits/master) [![coverage report](https://gitlab.com/rd.pradipta/personal-web/badges/master/coverage.svg)](https://gitlab.com/rd.pradipta/personal-web/commits/master)
